#!/usr/bin/python3

import json
import sys
import argparse
import os
import datetime
from datetime import datetime, timedelta,timezone
import uuid

#Parsing arguments

'''
---
description:
   gen_script utility generate data.json file with N minutes intervals and unique_id blocks in defined format.

options:
   start-time:
      description:
          - start date interval in format "YYYY-MM-DD HH:MM:SS"
      required: true

   end-time:
      description:
          - end date interval in format YYYY-MM-DD HH:MM:SS"
      required: true

example:
 gen_script.py --start-time "2023-04-01 12:08:00"    --end-time "2023-04-01 15:23:00"

'''

parser = argparse.ArgumentParser()

parser.add_argument('--start-time', dest='stime' , help="start date interval in format YYYY-MM-DD HH:MM:SS", required=True)
parser.add_argument('--end-time', dest='etime',  help="end date interval in format YYYY-MM-DD HH:MM:SS", required=True)

args = parser.parse_args()

start_time = getattr(args, 'stime')
end_time =  getattr(args, 'etime')


# checking if argumnets satisfied requed format
def check_args_format(dd, argument_name):

  try:
     d = datetime.strptime(dd, "%Y-%m-%d %H:%M:%S")  
  except:
    raise ValueError("Wrong  argument " + argument_name + " format,  should be YYYY-MM-DD HH:MM:SS")

def round_minutes(x):
    return 15 * round(x/15)    

# checking if minimum interval > 15 minutes
def validate_interval(dd1, dd2, min_interal):    
    minutes =(datetime.strptime(dd2, "%Y-%m-%d %H:%M:%S") - datetime.strptime(dd1, "%Y-%m-%d %H:%M:%S")).total_seconds()/60
    if  minutes  > min_interal:
        return 0
    elif minutes < 0: 
        print("Wrong interval parameters, end_time interval has to be after start_time!")
        sys.exit("Wrong interval!")
    else:
        print("Please enter interval larger then " + str(min_interal) + " minutes!")
        sys.exit("Wrong interval")

def round_minutes(x, delta):
    return delta * round(x/delta)

def normolize_interval(dd, delta):
    return dd.replace(minute=round_minutes(dd.minute, delta))


def generate_file(dd1,dd2,delta):
     data = []
     minutes_delta=timedelta(minutes=15)
     try:
        jsonFile = open("data.json", "w")
     except:
        raise "Error to open JSON file"
        sys.exit(1)

     while dd1 < dd2:
        
        t_dd1 = dd1.astimezone(timezone.utc).isoformat(timespec='seconds').replace('+00:00', 'Z')
        t_dd1_delta = (dd1 + minutes_delta).astimezone(timezone.utc).isoformat(timespec='seconds').replace('+00:00', 'Z')
        
        json_object = {"block_start_time": str(t_dd1), "block_end_time": str(t_dd1_delta),"unique_id": str(uuid.uuid4())}
        data.append(json_object)

        dd1+=minutes_delta
     try:
        json.dump(data, jsonFile, indent=22)
     except: 
        print("Error to write to json file") 
     finally:
        jsonFile.close()
     
def main():
   delta=15
  
   check_args_format(start_time,'start-time')
   check_args_format(end_time, 'end-time')
   validate_interval(start_time,end_time,delta)

   tf_start_time = normolize_interval(datetime.strptime(start_time, "%Y-%m-%d %H:%M:%S"), delta)
   tf_end_time   = normolize_interval(datetime.strptime(end_time, "%Y-%m-%d %H:%M:%S"), delta)
   
   generate_file(tf_start_time, tf_end_time, delta)

if __name__ == "__main__":
   main()
