# Python Script Documentation

## Introduction
This documentation provides an overview and usage guide for a Python script that accepts two command-line arguments, `start-time` and `end-time`, and generates a JSON array containing 15-minute blocks within the specified time interval.

## Table of Contents
- [Requirements](#requirements)
- [Input Validation](#input-validation)
- [Usage](#usage)
- [Time Interval Splitting](#time-interval-splitting)
- [JSON Output Structure](#json-output-structure)
- [Example](#example)

## Requirements<a name="requirements"></a>
To run the script, ensure that you have Python 3.x installed on your system.

## Input Validation<a name="input-validation"></a>
The script checks the validity of the provided timestamps and ensures that the difference between the `start-time` and `end-time` is at least 15 minutes. The following checks are performed:
1. Valid Time Format: The script verifies that the input arguments are in a valid time format using the ISO 8601 format.
2. Minimum Time Interval: The script ensures that the difference between the `start-time` and `end-time` is not less than 15 minutes.
If any of these checks fail, an error message is displayed, and the script terminates.


## Usage<a name="usage"></a>
To use the script, follow these steps:
1. Open a terminal or command prompt.
2. Navigate to the directory containing the Python script.
3. Run the script with the following command:

```
python script.py 2023-06-19T09:25:00Z 2023-06-19T10:10:00Z
```
4. Script will generate file data.json with defined output

## JSON Output Structure<a name="json-output-structure"></a>
For each 15-minute block within the specified time interval, the script generates a unique ID and creates an entry in the output JSON array. The structure of each JSON entry is as follows:
```json
{
"block_start_time": "2022-01-01T11:00:00Z",
"block_end_time": "2022-01-01T11:15:00Z",
"unique_id": "your_unique_id_of_choice"
}
```

**"block_start_time"**: The start time of the block.  
**"block_end_time"**: The end time of the block.  
**"unique_id"**: A unique identifier for the block.  

## Example
```
python script.py 2023-06-19T09:25:00Z 2023-06-19T10:10:00Z
```
